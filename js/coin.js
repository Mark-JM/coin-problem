//makeChange returns an array containing the ideal coin configuration
//(fewest number of coins) to yield a given value, e.g. [10, 2, 1] for 13p

var makeChange = function(coins) {

	var memo = {};
	
	return change = function(remainder) {
		if(!remainder) return [];
		if(memo[remainder]) return memo[remainder];

		var purse = [], newPurse;

		coins.forEach(function(coin) {
			newRemainder = remainder - coin;
			if(newRemainder >= 0) {
				//Constraint to find globally optimum solution rather than best local
				newPurseLength = (newPurse = change(newRemainder)).length;
				if(newPurseLength < purse.length-1 || !purse.length) {
					//Prevent short-changing (e.g. 5p given to make 6p when no 1ps available)
					if (newPurse.length || newRemainder === 0) {
						purse = [coin].concat(newPurse);
					}
				}
			}
		});
		return (memo[remainder] = purse);
	}
}

//countChange groups our change in to named piles, telling us how many of
//each particular coin we have, e.g. "2 x 10p, 1 x 1p" for 21p
var countChange = function(coins) {

	var purse = '',
	count = {},
	coinNames = {
		200: '£2',
		100: '£1',
		50: '50p',
		20: '20p',
		10: '10p',
		5: '5p',
		2: '2p',
		1: '1p'
	};

	coins.forEach(function(coin) {
		count[coin] = 1 + (count[coin] || 0);
	});
	
	for(var key in count) {
		purse = purse + count[key]+ ' x ' + coinNames[key] + '\n';
	}

	return purse;
}

//parseAmount tells us the exact value (in pennies) of a human currency input
//e.g. "187p" for £1.87  
var parseAmount = function(input) {
	var pounds = pennies = 0;
	
	//Return false if there is no input or if input does not match selection criteria
	if(!input || !input.match(/^([£]?\d*[.]?\d*[p]?)$/)) return false;

	//We can safely assume that if the input contains either "£" or "." then anything
	//before the "." is a pound value. Anything else must be a penny value.
	
	if(input.indexOf('£') !== -1 || input.indexOf('.') !== -1) {

		var parts = input.split('.'); //Split input at decimal point
		pounds = parseInt(parts[0].replace(/\D/g, ''), 10) || 0; //Remove any non-numeric characters

		//The penny segment
		if(parts[1]) {
			pennySegment = parts[1].replace(/\D/g, '');

			//If the penny value exceeds 2 digits, manually enter a decimal point and round the value
			if(pennySegment.length>2) {
				pennies = Math.round(pennySegment.substr(0, 2) + '.' + pennySegment.substr(2));
			}
			else {
				pennies = parseInt(pennySegment, 10) || 0;
			}
		}
	}
	else {
		pennies = parseInt(input, 10) || 0;
	}

	//Return the total sum (in pennies) of the amount given
	pence = (pounds*100) + pennies;
	return pence;
}