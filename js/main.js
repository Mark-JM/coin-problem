var allowedCoinValues = [
	200,
	100,
	50,
	20,
	10,
	5,
	2,
	1
];

var amountField = document.getElementById('amountField');
amountField.addEventListener('keyup', function(e) {
	if(e.which === 13) {
		var outputField = document.getElementById('outputField');
		var amount = parseAmount(amountField.value);
		if(amount) {
			var change = countChange(makeChange(allowedCoinValues)(amount));
			outputField.textContent = change.toString();
		}
		else {
			outputField.textContent = 'Sorry, you have entered an invalid amount!';
		}
	}
});