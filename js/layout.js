$(window).scroll(function() {

	//Used for sticky nav bar effect
	var nav = $('nav');
	var navPosition = nav.offset();

	var main = $('main');
	var mainPosition = main.position();

	if(navPosition.top + (nav.height()/2) >= mainPosition.top) {
		nav.addClass('active');
	}
	if(navPosition.top + (nav.height()/2) < mainPosition.top) {
		nav.removeClass('active');
	}

	if(navPosition.top >= mainPosition.top) {
		nav.addClass('canvased');
	}
	if(navPosition.top < mainPosition.top) {
		nav.removeClass('canvased');
	}
});

//Used for content panel pullout effects
$('.curious').on('click', function(ev) {
		$(this).parent().toggleClass('active');
});

$('.satisfied').on('click', function(ev) {
		$(this).parent().removeClass('active');
});