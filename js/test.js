test('makeChange()', function() {
	var coins = [50, 10, 5, 2, 1];
	deepEqual(makeChange(coins)(123), [50, 50, 10, 10, 2, 1], 'Large number');
	deepEqual(makeChange(coins)(4), [2, 2], 'Small number');
	deepEqual(makeChange(coins)(13), [10, 2, 1], 'Odd number');
	deepEqual(makeChange(coins)(14), [10, 2, 2], 'Even number');
	deepEqual(makeChange(coins)(0), [], 'Zero');
	deepEqual(makeChange([10, 5])(6), [], 'No remainder coins');
	deepEqual(makeChange([10, 6, 1])(12), [6, 6], 'Globally optimum search on non-standard coinset');
});

test('countChange()', function() {
	equal(countChange([1]), '1 x 1p\n', 'One coin');
	equal(countChange([1, 2]), '1 x 1p\n1 x 2p\n', 'Two different coins');
	equal(countChange([2, 2, 2]), '3 x 2p\n', 'Three same coins');
	equal(countChange([3]), '1 x undefined\n', 'Unnamed coin');
});

test('parseAmount()', function() {
	equal(parseAmount('4'), '4', 'Single digit');
	equal(parseAmount('85'), '85', 'Double digit');
	equal(parseAmount('197p'), '197', 'Pence symbol');
	equal(parseAmount('2'), '2', 'Pence symbol single digit');
	equal(parseAmount('1.87'), '187', 'Pounds decimal');
	equal(parseAmount('£1.23'), '123', 'Pound symbol');
	equal(parseAmount('£2'), '200', 'Single digit pound symbol');
	equal(parseAmount('£10'), '1000', 'Double Digit pound symbol');
	equal(parseAmount('£1.87'), '187', 'Pound and pence symbol');
	equal(parseAmount('£1'), '100', 'Missing pence');
	equal(parseAmount('£1.p'), '100', 'Missing pence but present decimal');
	equal(parseAmount('001.41p'), '141', 'Buffered zeros');
	equal(parseAmount('4.235p'), '424', 'Rounding three decimal places to two');
	equal(parseAmount('£1.257422457'), '126', 'Rounding with symbol');
	equal(parseAmount(''), false, 'Rejected: empty string');
	equal(parseAmount('1x'), false, 'Rejected: non-numeric character');
	equal(parseAmount('£1x.0p'), false, 'Rejected: non-numeric character');
	equal(parseAmount('£p'), false, 'Rejected: missing digits');
});